import json
import uuid
import sys
from datetime import datetime
import argparse

def unix_to_iso(unix_time):
    """Convert Unix timestamp to ISO 8601 format."""
    return datetime.utcfromtimestamp(unix_time).strftime('%Y-%m-%dT%H:%M:%SZ')

def extract_messages(mapping):
    """Extract messages from the mapping dictionary."""
    messages = []
    for key, value in mapping.items():
        message = value.get('message')
        if message:
            messages.append(message)
    return messages

def list_conversations(input_file):
    with open(input_file, 'r') as f:
        data = json.load(f)

    for i, conversation in enumerate(data):
        conversation_id = conversation.get('id', f'No ID {i}')
        title = conversation.get('title', 'No Title')
        create_time = conversation.get('create_time', 'No Create Time')
        if isinstance(create_time, (int, float)):
            create_time = unix_to_iso(create_time)
        short_id = hex(i)[2:]  # Use position as a hexadecimal number for shorter selectors
        print(f"Short ID: {short_id}, Full ID: {conversation_id}, Title: {title}, Created: {create_time}")

def transform_to_tiddlywiki(input_file, output_file, selected_ids=None, debug=False):
    with open(input_file, 'r') as f:
        data = json.load(f)

    if debug:
        print(f"Data structure: {type(data)}")

    tiddlers = []

    # Create metadata tiddler
    conversation_id = str(uuid.uuid4())
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")

    metadata_tiddler = {
        "title": "Metadata",
        "created": timestamp,
        "modified": timestamp,
        "tags": "metadata",
        "type": "application/json",
        "text": json.dumps({
            "conversation_id": conversation_id,
            "title": "Exported Conversations",
            "timestamp": timestamp
        })
    }
    tiddlers.append(metadata_tiddler)
    if debug:
        print(f"Added metadata tiddler: {metadata_tiddler}")

    for i, conversation in enumerate(data):
        conversation_id = conversation.get('id', 'No ID')
        short_id = hex(i)[2:]
        if selected_ids and short_id not in selected_ids:
            continue

        if debug:
            print(f"Conversation structure: {type(conversation)}")

        if 'create_time' in conversation and isinstance(conversation['create_time'], (int, float)):
            created_time = unix_to_iso(conversation['create_time'])
        else:
            print(f"Warning: 'create_time' missing or not a float in conversation: {conversation.get('id', 'unknown')}")
            created_time = datetime.now().strftime("%Y%m%d%H%M%S%f")

        if debug:
            print(f"Conversation create_time: {created_time}")

        mapping = conversation.get('mapping', {})
        messages = extract_messages(mapping)
        if debug:
            print(f"Messages structure: {type(messages)}")
            print(f"Messages content: {messages}")

        for j, message in enumerate(messages):
            content_parts = message.get('content', {}).get('parts', [])
            if isinstance(content_parts, list):
                content = ' '.join(
                    part if isinstance(part, str) else json.dumps(part)
                    for part in content_parts
                )
            else:
                content = str(content_parts)

            if debug:
                print(f"Message structure: {type(message)}")
                print(f"Message content: {content}")

            if 'author' not in message or 'role' not in message['author'] or 'create_time' not in message:
                print(f"Warning: Missing key(s) in message: {message}")
                continue

            role = message['author']['role']

            if isinstance(message['create_time'], (int, float)):
                timestamp = unix_to_iso(message['create_time'])
            else:
                print(f"Warning: 'create_time' missing or not a float in message: {message}")
                timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")

            tiddler_title = f"{role.capitalize()} Message {j + 1} (Conversation {short_id})"
            if debug:
                print(f"Tiddler title: {tiddler_title}")
                print(f"Tiddler timestamp: {timestamp}")
                print(f"Tiddler role: {role}")
                print(f"Tiddler content: {content}")

            tiddler = {
                "title": tiddler_title,
                "created": timestamp,
                "modified": timestamp,
                "tags": role,
                "type": "text/vnd.tiddlywiki",
                "text": content
            }
            tiddlers.append(tiddler)
            if debug:
                print(f"Added tiddler: {tiddler}")

    if debug:
        print(f"Total number of tiddlers: {len(tiddlers)}")

    with open(output_file, 'w') as f:
        json.dump(tiddlers, f, indent=4)
        print(f"Tiddlers written to {output_file}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Transform JSON to TiddlyWiki format.')
    parser.add_argument('input_file', help='Input JSON file')
    parser.add_argument('output_file', nargs='?', help='Output JSON file')
    parser.add_argument('--list', action='store_true', help='List conversations in the input file')
    parser.add_argument('--select', nargs='+', help='Short IDs of conversations to include in the output')
    parser.add_argument('--debug', action='store_true', help='Print debug information')

    args = parser.parse_args()

    if args.list:
        list_conversations(args.input_file)
    elif args.output_file:
        transform_to_tiddlywiki(args.input_file, args.output_file, selected_ids=args.select, debug=args.debug)
    else:
        parser.print_help()
